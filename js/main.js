//Import de la class ApiHelper
import { ApiHelper } from "./ApiHelper.js";
import { LANG } from "./lang.js";
console.log(LANG);
//Création d'un objet api_Helper

// console.log(api_Helper);
let urlparams = new URLSearchParams(window.location.search);
let param_lang = urlparams.get("lang");

if (!param_lang) {
  param_lang = "en";
}
let api_Helper = new ApiHelper();
//Appel de la methode
// let name_Helper = api_Helper.getName();
// console.log(name_Helper);

const BASE_URL = "https://vlr.orlandomm.net/api/v1/";

trad(param_lang);

//  Get select region
let selectRegion = document.getElementById("regions");
let regionsLabel = document.getElementById("regionsLabel");
// console.log(selectRegion);
// let teamOption  = document.querySelectorAll("#teams option");
let teamsDiv = document.getElementById("teamsDiv");
let myTeam = document.getElementById("teams");
let teamDiv = document.getElementById("teamDiv");
let playersDiv = document.getElementById("playersDiv");
// let playersOption = document.querySelectorAll("#players option");
let myPlayer = document.getElementById("players");
let playerDiv = document.getElementById("playerDiv");
let yearsDiv = document.getElementById("yearsDiv");
let myYear = document.getElementById("years");
// let tournois = document.getElementById('tournoi');
let team = null;
let gameYear = document.getElementById("games");

// Get lang select
let select_lang = document.getElementById("lang");
console.log("myteam " + myTeam);
let myServer = document.getElementById("server");

selectRegion.addEventListener("change", (event) => {
  //Récupération de la balise selectionée
  let [selectedOption] = selectRegion.selectedOptions;
  //console.log(selectedOption)
  let dataDoj = selectedOption.dataset.textkey;
  console.log(selectedOption.innerText);
  myServer.innerHTML = "SERVER : " + selectedOption.innerText;
  console.log(dataDoj);
  console.log("param_lang " + param_lang);
  let serveur = LANG[param_lang];
  console.log(serveur.dataDoj);
  // // Get API Request
  api_Helper.getTeamsByRegion(event.target.value).then((decoded) => {
    let eraseTeamsOption = myTeam.querySelectorAll("option");
    //  for (let element of eraseTeamsOption){
    //     element.remove();
    //  }
    // // console.log(decoded.data);
    for (let element of decoded.data) {
      let teamsOption = document.createElement("option");
      teamsOption.innerHTML = element["name"];
      teamsOption.setAttribute("value", element["id"]);
      myTeam.appendChild(teamsOption);
    }
    teamsDiv.classList.remove("hidden");
    selectRegion.classList.add("hidden");
    regionsLabel.classList.add("hidden");
  });
});

myTeam.addEventListener("change", (event) => {
  console.log(event.target.value);
  teamDiv.classList.toggle("hidden");
  teamsDiv.classList.add("hidden");
  // let apiTeam = BASE_URL + "teams/";
  // console.log(apiTeam);

  api_Helper.getTeamById(event.target.value).then((teamDec) => {
    team = teamDec;

    teamDiv.querySelector("#nom").innerHTML = teamDec.data.info.name;
    teamDiv.querySelector("img").setAttribute("src", teamDec.data.info.logo);
    teamDiv.querySelector("#tournoi").innerHTML = teamDec.data.events[0].name;
    teamDiv.querySelector("#an").innerHTML = teamDec.data.events[0].year;
    playersDiv.classList.remove("hidden");
    let erasePlayerOption = myPlayer.querySelectorAll("option");
    // for( let element of erasePlayerOption){
    //     element.remove();
    // }
    for (let elem of teamDec.data.players) {
      let choiceBetweenPlayers = document.createElement("option");
      choiceBetweenPlayers.innerHTML = elem.name;
      choiceBetweenPlayers.setAttribute("value", elem["id"]);
      myPlayer.appendChild(choiceBetweenPlayers);
    }
    let eraseYearOption = myYear.getElementsByTagName("option");

    console.log(eraseYearOption);

    let yearArray = [];

    for (let elem of teamDec.data.events) {
      yearArray.push(elem.year);
    }
    yearArray = [...new Set(yearArray)];
    for (let elem of yearArray) {
      let choiceBetweenYears = document.createElement("option");
      console.log(choiceBetweenYears);
      choiceBetweenYears.innerHTML = elem;
      // choiceBetweenPlayers.setAttribute("value", elem["id"]);
      myYear.appendChild(choiceBetweenYears);
    }
  });
});

myPlayer.addEventListener("change", (evenement) => {
  // console.log(evenement.target.value);
  api_Helper.getPlayerById(evenement.target.value).then((playerDec) => {
    // console.log(playerDec)
    playerDiv.classList.remove("hidden");
    playersDiv.classList.add("hidden");
    playerDiv.querySelector("img").setAttribute("src", playerDec.data.info.img);
    playerDiv.querySelector("#name").innerHTML = playerDec.data.info.name;
    playerDiv.querySelector("#nation").innerHTML = playerDec.data.info.country;
    playerDiv.querySelector("#pseudo").innerHTML = playerDec.data.info.user;
    yearsDiv.classList.remove("hidden");
  });
});

myYear.addEventListener("change", (evenement) => {
  yearsDiv.classList.add("hidden");
  gameYear.classList.remove("hidden");
  gameYear.querySelector("h3").innerHTML = evenement.target.value;
  for (let elem of team.data.events) {
    if (elem.year == evenement.target.value) {
      let myP = document.createElement("p");
      let results = document.createElement("p");
      console.log();
      myP.innerHTML = LANG[param_lang].competition_name + elem.name;
      results.innerHTML = "Resultats : " + elem.results;
      gameYear.appendChild(myP);
      gameYear.appendChild(results);
    }
  }
});
select_lang.addEventListener("change", (event) => {
  console.log(
    "++++++++++++++++++CHOOZEN LANG+++++++++++++++++++++ " + event.target.value
  );
  param_lang = event.target.value;
  trad(event.target.value);
});

function trad(code) {
  console.log("Function code : " + code);
  let traductible_elements = document.getElementsByClassName("trad");

  for (let elem of traductible_elements) {
    // console.log(LANG[code][elem.dataset.textkey])
    elem.innerText = LANG[code][elem.dataset.textkey];
  }
}
