export class ApiHelper {
  BASE_URL = "https://vlr.orlandomm.net/api/v1/";
  routes = {
    teamRoute: "teams",
  };

  players = {
    playersRoute: "players/",
  };

  async getTeamsByRegion(regionCode) {
    let api_url = this.BASE_URL + this.routes.teamRoute;
    api_url += "?region=" + regionCode;
    return fetch(api_url).then((response) => {
      return response.json();
    });
  }

  async getTeamById(teamId) {
    let api_url = this.BASE_URL + this.routes.teamRoute;
    api_url += "/";
    api_url += teamId;
    return fetch(api_url).then((response) => {
      return response.json();
    });
  }

  async getPlayerById(playerId) {
    let api_url = this.BASE_URL + this.players.playersRoute;
    api_url += playerId;
    return fetch(api_url).then((response) => {
      return response.json();
    });
  }
}
