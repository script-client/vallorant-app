export let LANG = {"fr":{
    "select_lang_tittle":"Choix de la langue",
    "lang_fr" : "Français",
    "lang_en" : "Anglais",
    "select_regions_tittle":"CHOISIR UNE REGION",
    "select_region_option_na":"Nord Américain",
    "select_region_option_eu":"Europe",
    "select_region_option_br":"Brésil",
    "select_region_option_ap":"Pacifique Australe",
    "select_region_option_kr":"Corée",
    "select_region_option_ch":"Chine",
    "select_region_option_jp":"Jap0n",
    "select_region_option_lan":"Amérique latine du nord",
    "select_region_option_las":"Amérique Latine du sud",
    "select_region_option_oce":"Océanie",
    "select_region_option_mn":"Mongolie",
    "select_region_option_gc":"Equipes féminines",
    "select_team_tittle":"CHOISIR UNE EQUIPE",
    "team_name":"Nom de l'équipe : ",
    "team_last_shown_comp": "Dernière apparition en championat : ",
    "year_last_comp":"Année du tournoi : ",
    "select_player_tittle" : "JOUEURS",
    "selected_player":"JOUEUR SELECTIONNE : ",
    "selected_player_country": "Pays d'origine : ",
    "selected_player_nick":"Pseudo : ",
    "select_year_tittle" : "CHOISIR UNE ANNEE POUR AFFICHER LES RESULTATS",
    "competition_name" : "nom du tournoi : "

    
},  "en":{
        "select_lang_tittle":"CHOOSE A LANGUAGE",
        "lang_fr" : "French",
        "lang_en" : "English", 
        "select_regions_tittle":"CHOOSE A REGION",
        "select_region_option_na":"Nord América",
        "select_region_option_eu":"Europe",
        "select_region_option_br":"Brazil",
        "select_region_option_ap":"Austral Pacific",
        "select_region_option_kr":"Corea",
        "select_region_option_ch":"China",
        "select_region_option_jp":"Japan",
        "select_region_option_lan":"North Latin America",
        "select_region_option_las":"South Latin America",
        "select_region_option_oce":"Océania",
        "select_region_option_mn":"Mongolia",
        "select_region_option_gc":"Girl Teams",
        "select_team_tittle":"CHOOSE A TEAMS",
        "team_name":"Team Name : ",
        "team_last_shown_comp" :"Team last competition : ",
        "year_last_comp":"Year of the competition : ",
        "select_player_tittle" : "PLAYERS",
        "selected_player":"SELECTED PLAYERS : ",
        "selected_player_country": "Born country : ",
        "selected_player_nick":"Nickname : ",
        "select_year_tittle" : "CHOOSE A YEAR TO DISPLAY RESULT",
        "competition_name" : "Competition name : "

    }
}